<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
    $kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"); // Lengkapi di sini
    $adults = array("Hopper", "Nancy",  "Joyce", "Jonathan", "Murray");
    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: " . count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";
    // Lanjutkan

    echo "</ol>";

    echo "Total Adults: " . count($adults); // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    // Lanjutkan

    echo "</ol>";

    /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"


            Output:
            Array
                (
                    [0] => Array
                        (
                            [Name] => Will Byers
                            [Age] => 12
                            [Aliases] => Will the Wise
                            [Status] => Alive
                        )

                    [1] => Array
                        (
                            [Name] => Mike Wheeler
                            [Age] => 12
                            [Aliases] => Dugeon Master
                            [Status] => Alive
                        )

                    [2] => Array
                        (
                            [Name] => Jim Hooper
                            [Age] => 43
                            [Aliases] => Chief Hopper
                            [Status] => Deceased
                        )

                    [3] => Array
                        (
                            [Name] => Eleven
                            [Age] => 12
                            [Aliases] => El
                            [Status] => Alive
                        )

                )
            
        */
    echo "<h3> Soal 3 </h3>";
    $arraymulti = array(
        array("Will Byers", 12, "Will the Wise", "Alive"),
        array("Mike Wheeler", 12, "Dungeon Master", "Alive"),
        array("Jim Hopper", 43, "Chief Hopper", "Deceased"),
        array("Eleven", 12, "El", "Alive")
    );
    echo "<pre>";
    echo "Output:<br>Array<br>\t(<br>\t\t[0] => Array<br>\t\t\t(
        \t\t\t[Name] => " . $arraymulti[0][0] . "
        \t\t\t[Age] => " . $arraymulti[0][1] . "
        \t\t\t[Aliases] => " . $arraymulti[0][2] . "
        \t\t\t[Status] => " . $arraymulti[0][3] . "
        \t\t)\n";
    echo "<br>\t\t[1] => Array<br>\t\t\t(
        \t\t\t[Name] => " . $arraymulti[1][0] . "
        \t\t\t[Age] => " . $arraymulti[1][1] . "
        \t\t\t[Aliases] => " . $arraymulti[1][2] . "
        \t\t\t[Status] => " . $arraymulti[1][3] . "
        \t\t)\n";
    echo "<br>\t\t[1] => Array<br>\t\t\t(
        \t\t\t[Name] => " . $arraymulti[2][0] . "
        \t\t\t[Age] => " . $arraymulti[2][1] . "
        \t\t\t[Aliases] => " . $arraymulti[2][2] . "
        \t\t\t[Status] => " . $arraymulti[2][3] . "
        \t\t)\n";
    echo "<br>\t\t[1] => Array<br>\t\t\t(
        \t\t\t[Name] => " . $arraymulti[3][0] . "
        \t\t\t[Age] => " . $arraymulti[3][1] . "
        \t\t\t[Aliases] => " . $arraymulti[3][2] . "
        \t\t\t[Status] => " . $arraymulti[3][3] . "
        \t\t)<br>\t)\n";
    echo "</pre>";
    ?>
</body>

</html>